//
//  TheMoviesAppTests.swift
//  TheMoviesAppTests
//
//  Created by Ignasi Casulà on 14/1/22.
//

import XCTest
@testable import TheMoviesApp

class TheMoviesAppTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let initialVC = HomeViewController.storyboardInstance()
        
        let viewModel = HomeViewModel(
            router: HomeRouter(),
            popularMoviesUseCase: MovieDBFetchPopularMoviesUseCase(
                repository: MovieDBPopularMoviesRepository()))
        
        initialVC.viewModel = viewModel
        let _ = initialVC.view
        
        viewModel.popularMovies.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }

}
